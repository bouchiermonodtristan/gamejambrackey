﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[AddComponentMenu("Camera-Control/Mouse Torque")]
[RequireComponent(typeof(Rigidbody))]

public class MenuController : MonoBehaviour {

    public GameObject MainCanvasGameObjectMoving;
    private float MainCanvasGameObjectMovingSaveX;
    private float MainCanvasGameObjectMovingSaveY;
    public float MainCanvasSensitivity = 10;
    public GameObject CanvasLockingRotation;
    private Vector2 screenCenter;

    private Transform MainCanvasTransformSave;
    private Rigidbody rigidbody;
    /** Controls how sensitive the horizontal axis is. */
    public float horizontalSensitivity = 30;

    /** Controls how sensitive the vertical axis is. */
    public float verticalSensitivity = 30;

    /** Controls how strongly the camera tries to keep itself upright. */
    public float correctiveStrength = 20;

    /** JoyStick Controll Mouse */
    public Vector2 CursorPosition;

    public GameObject CursorGameObject;

    private void Start()
    {
        rigidbody = this.GetComponent<Rigidbody>();
        MainCanvasGameObjectMovingSaveX = MainCanvasGameObjectMoving.transform.position.x;
        MainCanvasGameObjectMovingSaveY = MainCanvasGameObjectMoving.transform.position.y;
        screenCenter = new Vector2(Screen.width / 2, Screen.height / 2);
        CursorPosition = screenCenter;
    }


    public void moveMainCanvas(Vector2 PointerPosition)
    {
        float differenceX = (PointerPosition.x - screenCenter.x);
        float differenceY = (PointerPosition.y - screenCenter.y);
        MainCanvasGameObjectMoving.transform.position = new Vector3(
            MainCanvasGameObjectMovingSaveX - differenceX/MainCanvasSensitivity,
            MainCanvasGameObjectMovingSaveY - differenceY/MainCanvasSensitivity,
            0);
    }

    void FixedUpdate()
    {
        PointerEventData pointer = new PointerEventData(EventSystem.current);
        if (Mathf.Abs(Input.GetAxis("Mouse X")) > 0.1f || Mathf.Abs(Input.GetAxis("Mouse Y")) > 0.1f)
        {
            CursorPosition = Input.mousePosition;
        }
        if(CursorPosition.y > Screen.height)
        {
            CursorPosition = new Vector2(CursorPosition.x, Screen.height);
        }
        if (CursorPosition.y < 0)
        {
            CursorPosition = new Vector2(CursorPosition.x, 0);
        }
        if (CursorPosition.x > Screen.width)
        {
            CursorPosition = new Vector2(Screen.width, CursorPosition.y);
        }
        if (CursorPosition.x < 0)
        {
            CursorPosition = new Vector2(0, CursorPosition.y);
        }
        pointer.position = CursorPosition;

        CursorGameObject.transform.position = new Vector3(pointer.position.x, pointer.position.y, CursorGameObject.transform.position.z);
        moveMainCanvas(pointer.position);

        if ((Input.GetMouseButton(0) || Input.GetMouseButton(1))
            && CanvasLockingRotation.GetComponent<isMouseOver>().isMouseOverThisElement() == true)
        {

            List<RaycastResult> raycastResults = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointer, raycastResults);

            if (raycastResults.Count > 0)
            {
                bool isController = false;
                foreach (var go in raycastResults)
                {
                    if (go.gameObject.CompareTag("Controller") || go.gameObject.CompareTag("DestinyUI"))
                        isController = true;
                }
                if(!isController)
                    rigidbody.AddRelativeTorque(0, Input.GetAxis("Mouse X") * horizontalSensitivity, 0);
            }
        }
        transform.rotation = Quaternion.Euler(transform.localEulerAngles.x, transform.localEulerAngles.y, 0);
    }
}
