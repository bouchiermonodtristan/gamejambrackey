﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChangeCameraBackground : MonoBehaviour
{

    public GameObject Camera;


    public IEnumerator pickColor()
    {
        yield return new WaitForEndOfFrame();
        PointerEventData pointer = new PointerEventData(EventSystem.current);
        pointer.position = Input.mousePosition;

        Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
        texture.Apply();
        Color32 PixelColor = texture.GetPixel(Mathf.RoundToInt(pointer.position.x), Mathf.RoundToInt(pointer.position.y));
        Camera.GetComponent<Camera>().backgroundColor = PixelColor;
    }

    public void ChangeCameraBackgroundColor()
    {
        StartCoroutine(pickColor());
    }

}
