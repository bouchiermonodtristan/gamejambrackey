﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteData : MonoBehaviour
{
    public DataController PlayerData;
    private void Start()
    {
        PlayerData = GameObject.Find("SaveManager").GetComponent<DataController>();
    }
    public void deleteData()
    {
        PlayerData.PlayerPrefDeleteAll();
       // GameObject.Find("GameManager").GetComponent<GameManager>().resetGame();
    }
}
