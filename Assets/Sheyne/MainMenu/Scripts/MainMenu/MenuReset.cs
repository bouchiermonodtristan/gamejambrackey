﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.PostProcessing;

public class MenuReset : MonoBehaviour
{
    PlayerProgress playerProgress;
    GameObject CameraMain;
    public GameObject UIManagerGameObject;
    public Material skybox;
    public GameObject SkyboxColor;
    public PostProcessingProfile DefaultPostProcessingProfile;

    private void Start()
    {
        CameraMain = GameObject.FindGameObjectWithTag("MainCamera");
        playerProgress = GameObject.Find("SaveManager").GetComponent<PlayerProgress>();
        resetGameSetting();
    }

    public void resetGameSetting()
    {
        resetEffect();
    }

    private void resetEffect()
    {
        resetSkyboxColor();
        ResetPostProcessing();
    }

    public void resetSkyboxColor()
    {
        playerProgress.saveSettingBackground(255, 255, 255, 255);
        skybox.SetColor("_SkyTint", new Color32(255, 255, 255, 255));
    }
     public void ResetPostProcessing()
    {
        playerProgress.saveSettingPostProcessing(3);
        CameraMain.GetComponent<PostProcessingBehaviour>().profile = DefaultPostProcessingProfile;
    }
}
