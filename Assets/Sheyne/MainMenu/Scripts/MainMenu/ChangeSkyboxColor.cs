﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChangeSkyboxColor : MonoBehaviour
{
    public Material skybox;
    PlayerProgress playerProgress;

    private void Start()
    {
        playerProgress = GameObject.Find("SaveManager").GetComponent<PlayerProgress>();
        loadColor();
    }
    public void loadColor()
    {
        if (playerProgress.dataController.isPlayerPref("SettingBackgroundColor"))
        {
            Color32 PixelColor = playerProgress.SettingBackground;
            skybox.SetColor("_SkyTint", PixelColor);
        }
    }
    public IEnumerator pickColor()
    {
        yield return new WaitForEndOfFrame();
        PointerEventData pointer = new PointerEventData(EventSystem.current);
        pointer.position = Input.mousePosition;

        Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
        texture.Apply();
        Color32 PixelColor = texture.GetPixel(Mathf.RoundToInt(pointer.position.x), Mathf.RoundToInt(pointer.position.y));
        skybox.SetColor("_SkyTint", PixelColor);
        playerProgress.saveSettingBackground(PixelColor.r, PixelColor.g, PixelColor.b, PixelColor.a);
    }

    public void ChangeSkyboxBackgroundColor()
    {
        StartCoroutine(pickColor());
    }
}