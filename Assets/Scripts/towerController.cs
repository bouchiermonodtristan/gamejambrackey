﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class towerController : MonoBehaviour
{
	[Header("Stat Tour :")]
	public float rayonAttack;
	public float vitesseAttack;
	public float vitesseCast;

	public enum TowerType { Feux, Glace, Electricite }
	public TowerType towerType;

	[Header("Info exterieur")]
	public GameObject[] gameObjectsEnnemy;

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		//trouve tout les ennemies instancier
		//TODO remplasser par une list dans le GameController
		//GameObject[] gameObjectsEnnemy = GameObject.FindGameObjectsWithTag("Ennemy");

		Transform rayonAttackSprite = gameObject.transform.Find("towerRange");
		rayonAttackSprite.localScale = new Vector3(rayonAttack, rayonAttack, rayonAttack);

		if (EnnemieProche())
		{
			Attack(EnnemieLePlusProche());
		}

	}

	private bool EnnemieProche()
	{
		return false;
	}

	private GameObject EnnemieLePlusProche()
	{
		return null;
	}

	private void Attack(GameObject enemy)
	{
		//todo
	}
}
