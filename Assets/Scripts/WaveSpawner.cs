﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{
    //Ennemies
    [SerializeField]
    private Transform ennemyPrefab;
    //Temps entre chaque vague
    [SerializeField]
    private float timeBetweenWaves = 5f;
    //Durée de la vague
    private float countDown = 2f;
    private int waveNumber = 0;
    //Start de la vague
    [SerializeField]
    private Transform spawnPoint;

    [SerializeField]
    private Text waveCountDownTimer;

    // Update is called once per frame
    void Update()
    {
        if (countDown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countDown = timeBetweenWaves;
        }

        countDown -= Time.deltaTime;
        waveCountDownTimer.text = Mathf.Round(countDown).ToString();
    }

    public IEnumerator SpawnWave()
    {
        waveNumber++;
        for (int i = 0; i < waveNumber; i++)
        {
            SpawnEnnemy();
            //Temps entre chaque ennemies
            yield return new WaitForSeconds(0.5f);
        }        
    }

    public void SpawnEnnemy()
    {
        Instantiate(ennemyPrefab, spawnPoint.position, spawnPoint.rotation);
    }
}
